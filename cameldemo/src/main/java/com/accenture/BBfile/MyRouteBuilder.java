package com.accenture.BBfile;

import org.apache.camel.builder.RouteBuilder;

public class MyRouteBuilder extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		
		from("file:entrada?noop=true")
		.to("file:salida");
		
	}

}
