package com.accenture.CCprodcon;

import org.apache.camel.builder.RouteBuilder;

public class CustomRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		
		 from("direct:start")
		.process(new MyProcessor())
		.to("seda:end");
		
	}

}
