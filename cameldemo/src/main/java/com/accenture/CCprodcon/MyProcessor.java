package com.accenture.CCprodcon;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class MyProcessor implements Processor {

	public void process(Exchange exchange) throws Exception {

		String message = exchange.getIn().getBody(String.class);
		
		message = message + " -mensaje procesado por Speicys";
		
		exchange.getOut().setBody(message);
		
	}

}
