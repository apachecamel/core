package com.accenture.DDactivemq;

import org.apache.camel.builder.RouteBuilder;

public class MyRouterBuilder4 extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		
		from("file:entrada?noop=true")
		.to("activemq:queue:mi_queue");
		
	}

}
