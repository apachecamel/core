package com.accenture.FFmultitemplates;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class Procesador implements Processor {

	public void process(Exchange exchange) throws Exception {
		
		String message = exchange.getIn().getBody(String.class);
		
		message = message + " -mensaje procesado- ";
		
		exchange.getOut().setBody(message);
		
	}

}
