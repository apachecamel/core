package com.accenture.FFmultitemplates;

import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

class Usuario {
	
	String nombre;
	String secretmsg;
	int id;
}

public class camelMain {
	
	public static void main(String[] args) throws Exception {
		
		Usuario ignacio = new Usuario();
		
		ignacio.nombre = "ignacio";
		ignacio.secretmsg = "speicys";
		ignacio.id = 1;
		
		CamelContext context = new DefaultCamelContext();
		
		context.addRoutes(new RouteBuilder() {
			
			@Override
			public void configure() throws Exception {
				
				from("direct:start").process(new Procesador()).to("seda:end");
				from("direct:start2").process(new ProcesadorAlternativo()).to("seda:end2");
				from("direct:start3").to("seda:end3");
				
			}
		});
		
		context.start();
		
		ProducerTemplate producerTemplate = context.createProducerTemplate();
		producerTemplate.sendBody("direct:start", "msj creado desde producerTemplate -");
		producerTemplate.sendBody("direct:start2", "msj 2 -");
		producerTemplate.sendBody("direct:start3", ignacio);
		
		ConsumerTemplate consumerTemplate = context.createConsumerTemplate();
		String message = consumerTemplate.receiveBody("seda:end", String.class);
		String message2 = consumerTemplate.receiveBody("seda:end2", String.class);
		Usuario receiver = consumerTemplate.receiveBody("seda:end3", Usuario.class);
		
		System.out.println(message);
		
		System.out.println(message2);
		
		System.out.println(receiver.nombre);
		
	}

}
