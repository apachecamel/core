package com.accenture.HHcamelchoice;

import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;


class Persona {
	
	String nombre;
	String secretmsg;
	int id;
}

//para probar .choice, .when, etc..

public class camelMainHH {

	public static void main(String[] args) throws Exception {
		
		String mensaje = "nacho";
		
		CamelContext context = new DefaultCamelContext();
		
		context.addRoutes(new RouteBuilder() {
			
			@Override
			public void configure() throws Exception {
				
				from("direct:a")
				.choice()
					.when(body().isEqualTo("nacho"))
					.process(new Processor() {
						
						public void process(Exchange exchange) throws Exception {
							
							String receptor = exchange.getIn().getBody(String.class);
							
							receptor = "bienvenido nacho";
							
							exchange.getOut().setBody(receptor);
						}
					})
						.to("seda:b")
					.otherwise()
					.process(new Processor() {
						
						public void process(Exchange exchange) throws Exception {
							
							String receptor = exchange.getIn().getBody(String.class);
							
							receptor = "mensaje modificado en el camino 2";
							
							exchange.getOut().setBody(receptor);
						}
					})
						.to("seda:b");
				
			}
		});
		
		context.start();
		
		ProducerTemplate producerTemplate = context.createProducerTemplate();
		producerTemplate.sendBody("direct:a", mensaje);
		
		ConsumerTemplate consumerTemplate = context.createConsumerTemplate();
		String receiver = consumerTemplate.receiveBody("seda:b", String.class);
		
		System.out.println(receiver);
		
		

	}

}
