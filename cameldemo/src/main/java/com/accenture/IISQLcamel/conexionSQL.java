package com.accenture.IISQLcamel;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.SimpleRegistry;

import com.mysql.cj.jdbc.MysqlDataSource;

//import com.mysql.cj.jdbc.MysqlDataSource;

public class conexionSQL {

	public static void main(String[] args) throws Exception {

		// creo un objeto DataSource con los datos de la base de datos
		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setURL("jdbc:mysql://localhost:3306/camel_tutorial");
		dataSource.setUser("root");
		dataSource.setPassword("1234");

		// Instancio un SimpleRegistry y abajo le cargo los datos del data src anterior
		SimpleRegistry registry = new SimpleRegistry();
		registry.put("myDataSource", dataSource);

		CamelContext context = new DefaultCamelContext(registry);

		context.addRoutes(new RouteBuilder() {

			@Override
			public void configure() throws Exception {
				
				from("direct:start")
				.to("jdbc:myDataSource")
				.bean(new manejadorRdos(), "PrintResult");
				
			}
		});
		
		context.start();
		
		ProducerTemplate producerTemplate = context.createProducerTemplate();
		producerTemplate.sendBody("direct:start", "select * from customer");

	}

}
